prod:
	docker-compose up -d api nginx backend

generate-statics:
	docker-compose run frontend npm run build
	cp -r frontend/dist frontend/prod

dev:
	docker-compose up -d api frontend backend
