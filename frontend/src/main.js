import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import router from './router'
import VueResource from 'vue-resource';
import VueCookie from 'vue-cookie';

Vue.use(VueResource);

Vue.http.interceptors.push((request, next) => {
  const csrftoken = Vue.cookie.get('token')

  if (csrftoken) {
    request.headers.set('Authorization', `Token ${csrftoken}`)

    next((response) => {

      if (response.status === 401) {
        if (response.data.detail === 'Invalid token.') {
          console.log("Expirar cookies")
          document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/'
          document.location.replace('/login')
        }
      }
      else
      {
        console.log("Tengo cookies, puedo seguir")
        next()
      }
    })
  }
  else if (request.url === 'http://localhost:8000/api-token-auth/') {
    next()
  } 
  else
  {
    console.log("No tengo coockies")
    document.location.replace('/login')
  }
})


Vue.http.headers.common['access-control-allow-origin'] = '*';
Vue.http.headers.common['access-Control-Allow-Credentials'] = 'true';
Vue.http.headers.common['access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS, HEAD';
Vue.http.headers.common['access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization, access-control-allow-origin';
library.add(fas)
library.add(fab)
library.add(far)

Vue.component('fa-icon', FontAwesomeIcon)
Vue.use(BootstrapVue)
Vue.use(VueCookie)

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
