export const messages = {   
    en: {     
      navbar: {       
        inicio: 'Home',
        producto: 'Product',
        galeria: 'Gallery',
        empresa: 'Company',
        contacto: 'Contact'
      },
      footer: {
        nosotros: 'About us',
        aviso: 'Legal warning',
        politica: 'Legal policy',
        privacidad: 'Privacy policy'
      },
      video:{
        colocar_contenido: "The placement method is very simple, fast and with little material loss. In this short video we show you the basic steps for a perfect placement of our stone panels",
        colocar_titulo: "How do you placed it"
      },
      producto: {
        bigpanel: {
          content1: '',
          content2: '',
        },
        lightpanel: {
          content1: '',
          content2: '',
        },
        bigblack: 'Slate',
        bigmarfil: 'Travertine marble',
        cuarcitaoxidada: 'Rusted quartzite',
        granitooxidado: 'Rusted granite',
        pizarraoxidada: 'Rusted slate',
        acabado: "finished natural",
        acabadocizallado: 'sheared natural finish',
        precio: 'For an exact price ask for a quote the marked price is an approximation',
        propiedades: {
          natural: 'natural',
          dimensiones: 'Dimensions',
          tipopiedra: 'Type of stone',
          ambientes: 'Interior and exterior environments',
          pedidominimo: 'Minimum order',
          base: 'Base',
          esquinalarga: 'Long corner',
          esquinacorta: 'Short corner'
        },
        tipo_piedra:{
          pizarra: 'Slate',
          granito: 'Granite',
          cuarcita: 'Quartzite',
          marmol: 'Marble',
          marfil: 'Ivory'
        }
      },     
      empresa: {
        quienessomos: {
          title: 'Who are we?',
          content: 'StoneVision is dedicated to import and export of natural stone for walls and floors covering. We have an expert team of professionals with a long experience in construction projects in Spain, so the client may received the best advice depending on the region, climate and many other factors that are involved in the correct choice of material.'
        },
        objetivo: {
          title: 'Objective',
          content: 'The objective of StoneVision is the positioning in the import and export market of natural stone for covering, by offering the chance that the efficiency and design of natural stone reach the whole world, based on the selection of the best materials and total transparency towards our customers and suppliers.'
        },
        garantia: {
          title: 'Supply guarantee',
          content: 'StoneVision is based on the exhaustive search of the best suppliers and materials, requiring a quality guarantee of each of our models. It may be achieved through ice and thaw tests and thermal and acoustic impacts. In this way maximum effectiveness of placement, design and durability may be guaranteed.'
        }
      },
      cardcontact: {
        title: 'Contact data',
        telefono: 'Phone',
        email: 'Email',
        rrss: 'Social network',
        descripcion: 'For more information please do not hesitate to contact us through any of our communication channels.'
      },    
      carousel: {
        title: 'Design and Efficiency',
        description: 'Natural stone panels for interior and exterior wall cladding with the greatest guarantees of installation, insulation and design'
      }
    },   
    es: {     
      navbar: {       
        inicio: 'Inicio',
        producto: 'Producto',
        galeria: 'Galería',
        empresa: 'Empresa',
        contacto: 'Contacto'
      },
      footer: {
        nosotros: 'Sobre nosotros',
        aviso: 'Aviso legal',
        politica: 'Política legal',
        privacidad: 'Política de privacidad'
      },
      video:{
        colocar_contenido: "El método de colocación es muy sencillo, rápido y con poca merma de material. En este breve video te enseñamos los pasos básicos para una perfecta colocación de nuestro paneles de piedra",
        colocar_titulo: "Cómo se coloca"
      },
      producto: {
        bigpanel: {
          content1: '',
          content2: '',
        },
        lightpanel: {
          content1: '',
          content2: '',
        },
        bigmarfil: 'Mármol Travertino',
        cuarcitaoxidada: 'Granito oxidado',
        cuarcitaoxidada: 'Cuarcita oxidada',
        pizarraoxidada: 'Pizarra oxidada',
        granitooxidado: 'Granito oxidado',
        acabadocizallado: 'Acabado natural cizallado',
        acabado: "acabado natural",
        precio: 'Para un precio exacto solicitar cotización el precio marcado es una aproximación',
        propiedades: {
          natural: 'natural',
          dimensiones: 'Dimensiones',
          tipopiedra: 'Tipo de piedra',
          ambientes: 'Ambientes Exteriores e Interiores',
          pedidominimo: 'Pedido mínimo',
          base: 'Base',
          esquinalarga: 'Esquina larga',
          esquinacorta: 'Esquina corta'
        },
        tipo_piedra:{
          pizarra: 'Pizarra',
          granito: 'Granito',
          cuarcita: 'Cuarcita',
          marmol: 'Mármol',
          marfil: 'Marfil'
        }
      }, 
      empresa: {
        quienessomos: {
          title: '¿Quienes somos?',
          content: 'En StoneVision nos dedicamos a la importación y exportación de piedra natural para revestimiento de muros y suelos. Contamos con un equipo de personas formadas en España con una extensa trayectoria en todo tipo de proyectos para fachadas e interiores, para poder ofrecer al cliente el mejor asesoramiento dependiendo de la región, el clima y otros muchos factores que intervienen para una correcta elección del material.'
        },
        objetivo: {
          title: 'Objetivo',
          content: 'El objetivo de StoneVision es el posicionamiento en el mercado de la importación y exportación de piedra natural para revestimientos, desarrollando así la posibilidad de hacer llegar a todo el mundo el diseño y la eficacia de la piedra natural, basándonos en la selección de los mejores materiales y la total transparencia hacia nuestros clientes y proveedores.'
        },
        garantia: {
          title: 'Garantía de suministro',
          content: 'StoneVision tiene como piedra angular el estudio exhaustivo de proveedores y materiales exigiendo así una garantía de calidad de cada uno de nuestros modelos mediante ensayos de hielo y deshielo e impactos térmicos y acústicos. De esta forma garantizamos la máxima eficacia de colocación, diseño y durabilidad.'
        }
      }, 
      cardcontact: {
        title: 'Datos de contacto',
        telefono: 'Teléfono',
        email: 'Email',
        rrss: 'Redes sociales',
        descripcion: 'Para más información por favor no dude en ponerse en contacto con nosotros a través de cualquiera de nuestros canales de comunicación'
      },
      carousel: {
        title: 'Diseño y eficacia',
        description: 'Paneles de piedra natural para revestimiento de muros interiores y exteriores con las mayores garantías de colocación, aislamiento y diseño'
      }   
    } 
  }