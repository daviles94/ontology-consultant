import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Pages/Login.vue'
import Main from '../components/Pages/Main.vue'

Vue.use(VueRouter)

export default new VueRouter({
    mode:'history',
    routes: [
    {
        path: '/login',
        name: 'Login',
        component: Login
    },

    {
        path: '/',
        name: 'Main',
        component: Main
    }

    ]
})