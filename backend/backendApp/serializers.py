from django.db import transaction
from .models import User, QuerySaved
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'username')


class QuerySavedSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuerySaved
        fields = ('pk', 'name', 'query')