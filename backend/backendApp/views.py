from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from .serializers import UserSerializer, QuerySavedSerializer
from .models import User, QuerySaved
from rest_framework.decorators import action
from django.db.models import Avg, Count, Min, Sum



class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(detail=False)
    def auth(self, request, *args, **kwargs):
        instance = self.queryset.get(username=request.user)
        serializer = self.get_serializer(instance)
        return Response({'user': serializer.data})


class QuerySavedViewSet(viewsets.ModelViewSet):
    """
    Actions as views related to accounts
    """
    queryset = QuerySaved.objects.all()
    serializer_class = QuerySavedSerializer

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)


    def perform_create(self, serializer):
        serializer.save(owner=self.request.user) 
