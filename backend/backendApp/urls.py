from django.urls import path, include
from rest_framework.routers import DefaultRouter
from backendApp import views

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'querysaved', views.QuerySavedViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
]
