package controller;

import com.complexible.common.rdf.query.resultio.TextTableQueryResultWriter;
import com.complexible.stardog.StardogException;
import com.complexible.stardog.api.*;
import com.complexible.stardog.api.admin.AdminConnection;
import com.complexible.stardog.api.admin.AdminConnectionConfiguration;
import com.complexible.stardog.protocols.http.JSON;
import com.stardog.stark.query.SelectQueryResult;
import com.stardog.stark.query.io.QueryResultFormats;
import com.stardog.stark.query.io.QueryResultWriters;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class getResultFromQuery {
	private static boolean reasoningType = false;
	private static int maxPool = 200;
	private static int minPool = 10;

	private static long blockCapacityTime = 900;
	private static TimeUnit blockCapacityTimeUnit = TimeUnit.SECONDS;
	private static long expirationTime = 300;
	private static TimeUnit expirationTimeUnit = TimeUnit.SECONDS;

	public static String executeQuery(String query) {
		Properties fp = new Properties();
		try {
			fp.load(new FileInputStream("resources/configuration.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
		String bd = fp.getProperty("bd");
		String username = fp.getProperty("username");
		String password = fp.getProperty("password");
		String url = fp.getProperty("url");
		
		createAdminConnection(username, password, url); // creates the admin connection to perform some administrative actions
		ConnectionConfiguration connectionConfig = ConnectionConfiguration.to(bd).server(url).reasoning(reasoningType)
				.credentials(username, password);
		ConnectionPool connectionPool = createConnectionPool(connectionConfig); // creates the Stardog connection pool

		try (Connection connection = getConnection(connectionPool)) { // obtains a Stardog connection from the pool

			try {
				connection.begin();
				connection.commit();

				SelectQuery query_cast = connection.select(query);
				SelectQueryResult queryResult = query_cast.execute();
//				QueryResultWriters.write(queryResult, System.out, TextTableQueryResultWriter.FORMAT);
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				QueryResultWriters.write(queryResult, outputStream, QueryResultFormats.JSON);
				releaseConnection(connectionPool, connection);
				connectionPool.shutdown();
				
				String json = new String(outputStream.toByteArray());
				
				return json.toString();

			} catch (StardogException | IOException e) {
				e.printStackTrace();
				releaseConnection(connectionPool, connection);
				connectionPool.shutdown();
			}	
		}
		return null;
	}

	/**
	 * Creates a connection to the DBMS itself so we can perform some administrative
	 * actions.
	 */
	public static void createAdminConnection(String username, String password, String url) {
		try (final AdminConnection aConn = AdminConnectionConfiguration.toServer(url).credentials(username, password)
				.connect()) {
			aConn.close();
		}
	}

	/**
	 * Now we want to create the configuration for our pool.
	 * 
	 * @param connectionConfig the configuration for the connection pool
	 * @return the newly created pool which we will use to get our Connections
	 */
	private static ConnectionPool createConnectionPool(ConnectionConfiguration connectionConfig) {
		ConnectionPoolConfig poolConfig = ConnectionPoolConfig.using(connectionConfig).minPool(minPool).maxPool(maxPool)
				.expiration(expirationTime, expirationTimeUnit)
				.blockAtCapacity(blockCapacityTime, blockCapacityTimeUnit);

		return poolConfig.create();
	}

	/**
	 * Obtains the Stardog connection from the connection pool
	 * 
	 * @param connectionPool the connection pool to get our connection
	 * @return Stardog Connection
	 */
	public static Connection getConnection(ConnectionPool connectionPool) {
		return connectionPool.obtain();
	}

	/**
	 * Releases the Stardog connection from the connection pool
	 * 
	 * @param connection Stardog Connection
	 */
	public static void releaseConnection(ConnectionPool connectionPool, Connection connection) {
		try {
			connectionPool.release(connection);
		} catch (StardogException e) {
			e.printStackTrace();
		}
	}
}