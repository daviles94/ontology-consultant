package controller;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.ModelFactory;

import queries.QueryImpl;

public class Functions {

	public static String getDatatype(String Class) {
		Properties fp = new Properties();
		try {
			fp.load(new FileInputStream("resources/configuration.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		String ontologyDir = fp.getProperty("ontologyDir");
		String ontologyName = fp.getProperty("ontologyName");
		
		String SOURCE = ontologyDir + ontologyName;

		// create a model using reasoner
		OntModel model1 = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);

		// read the RDF/XML file
		model1.read(SOURCE, "RDF/XML");

		// Create a new query
		String queryGetObjectProperties = QueryImpl.getQueryGetDataProperties(Class);
		Query query = QueryFactory.create(queryGetObjectProperties);

		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.create(query, model1);
		ResultSet resultSet = qe.execSelect();

		// write to a ByteArrayOutputStream
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		// set option as JSON
		ResultSetFormatter.outputAsJSON(outputStream, resultSet);
		qe.close();

		// and turn that into a String
		String json = new String(outputStream.toByteArray());

		return json;
	}

	public static String getObjectProperty(String Class) {
		Properties fp = new Properties();
		try {
			fp.load(new FileInputStream("resources/configuration.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		String ontologyDir = fp.getProperty("ontologyDir");
		String ontologyName = fp.getProperty("ontologyName");
		
		String SOURCE = ontologyDir + ontologyName;
		
		// create a model using reasoner
		OntModel model1 = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);

		// read the RDF/XML file
		model1.read(SOURCE, "RDF/XML");

		// Create a new query
		String queryGetObjectProperties = QueryImpl.getQueryGetObjectProperties(Class);
		Query query = QueryFactory.create(queryGetObjectProperties);

		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.create(query, model1);
		ResultSet resultSet = qe.execSelect();

		// write to a ByteArrayOutputStream
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		// set option as JSON
		ResultSetFormatter.outputAsJSON(outputStream, resultSet);
		qe.close();

		// and turn that into a String
		String json = new String(outputStream.toByteArray());

		return json;
	}

}
