package services;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

@Path("returnFirstClass")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ReturnFirstClass {

	@GET
	public Response getFirstClass() {
		JSONObject jo = new JSONObject();
		
		Properties fp = new Properties();
		try {
			fp.load(new FileInputStream("resources/configuration.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		String parentClass = fp.getProperty("parentClass");
		String parentClassNS = fp.getProperty("parentClassNS");
		
		jo.put("clase", parentClass);
		jo.put("ns", parentClassNS);
		return Response.ok(jo.toString(), MediaType.APPLICATION_JSON).build();
	}
}