package services;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import controller.Functions;

@Path("getDataType")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DataTypeService {

	@GET
	public Response getDataType(@QueryParam("clase") String clase) {
		clase = "<".concat(clase.replace("___", "#").concat(">"));
		return Response.ok(Functions.getDatatype(clase), MediaType.APPLICATION_JSON).build();
	}
}