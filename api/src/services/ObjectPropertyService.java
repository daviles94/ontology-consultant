package services;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import controller.Functions;

@Path("getObjectProperty")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ObjectPropertyService {

	@GET
	@OPTIONS
	public Response getObjectProperty(@QueryParam("clase") String clase) {
		clase = "<".concat(clase.replace("___", "#").concat(">"));
		return Response.ok(Functions.getObjectProperty(clase), MediaType.APPLICATION_JSON).build();
	}
}