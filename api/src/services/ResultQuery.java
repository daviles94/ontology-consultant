package services;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import controller.Functions;
import controller.getResultFromQuery;

@Path("getResultQuery")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ResultQuery {

	@POST
	@OPTIONS
	public Response getResultQuery(String response) {
		String query = response.split("\":\"")[1].replace("\\n", " ").replace("\\t", " ").replace("\"}", ""); 
		
		System.out.println("-------------QUERY EXECUTED-------------");
		System.out.println(query);
		
		String result = getResultFromQuery.executeQuery(query);
		System.out.println("-------------RESULT-------------");
		System.out.println(result);

		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}
}