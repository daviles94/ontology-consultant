package queries;

public class QueryImpl {

	static String prefix = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\r\n"
			+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n"
			+ "PREFIX owl: <http://www.w3.org/2002/07/owl#>\r\n";

	public static String getQueryGetObjectProperties(String className) {
		String queryGetObjectPropertiesFromClass = prefix + "SELECT ?property ?range\n" + "WHERE {\n"
				+ " ?property rdfs:domain " + className + ";\n" + " rdf:type owl:ObjectProperty;\n"
				+ " rdfs:range ?class.\n" + "   ?range rdfs:subClassOf* ?class.\n" + " }";

		return queryGetObjectPropertiesFromClass;
	}

	public static String getQueryGetDataProperties(String className) {
		String queryGetDataPropertiesFromClass = prefix
				+ "SELECT ?property ?range (GROUP_CONCAT(?list;SEPARATOR=\"||\") AS ?optionList)\r\n" + "WHERE {\r\n"
				+ "?property rdfs:domain " + className + ";\r\n" + "rdf:type owl:DatatypeProperty.\r\n"
				+ "OPTIONAL{?property rdfs:range [ owl:onDatatype  ?range].}\r\n"
				+ "OPTIONAL{?property rdfs:range [ owl:oneOf [ rdf:rest* [rdf:first ?list]]].}\r\n"
				+ "OPTIONAL{?property rdfs:range [ owl:oneOf [ rdf:type  ?range]].}\r\n"
				+ "OPTIONAL{?property rdfs:range ?range.}\r\n" + "}\r\n" + "GROUP BY ?property ?range";
		return queryGetDataPropertiesFromClass;
	}

}
